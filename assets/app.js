function App(containerId, width, height) {
    var _this = this;
    this.nodes = {};
    this.mode = 'cursor';
    this.step = 16;
    this.width = width;
    this.height = height;
    this.draw = SVG('container').size(width, height);
    this.grid = this.draw.path('M 0 0').stroke({width: 1, color: '#e5e5e5'});
    this.linkGroup = this.draw.group();
    this.link0Group = this.draw.group();
    this.carGroup = this.draw.group();
    this.line = this.draw.line(0, 0, 0, 0).stroke({width: 2, color: 'red'}).hide();
    this.indexNode = 0;
    this.hoverNode = null;
    this.lastNode = null;
    this.selected = null;
    this.cars = [];
    this.timerId = null;
    this.tick = 0;
    this.speed = 1;
    this.status = '';

    this.drawGrid = function () {
        let step = this.step;
        let width = Math.ceil(this.width / step) - 1;
        let height = Math.ceil(this.height / step) - 1;
        let p = 'M ' + width * step + ' 0 ' + width * step + ' ' + height * step;
        p += 'M  0 ' + height * step + ' ' + width * step + ' ' + height * step;
        for (let x = 0; x < width; x++)
            p += ' M ' + x * step + ' 0 L ' + x * step + ' ' + height * step;

        for (let y = 0; y < height; y++)
            p += ' M 0 ' + y * step + ' L ' + width * step + ' ' + y * step;
        this.grid.plot(p);
    }

    this.setMode = function(mode) {
        this.mode = mode;
        if (mode !== 'cursor' && this.selected) {
            this.selected.deselect();
            this.selected = null;
        }
        if (mode === 'node') {
            this.lastNode = null;
            this.draw.style('cursor',  'crosshair');
        }
        else {
            this.draw.style('cursor',  'default');
        }
    }

    this.update = function () {
        for (let i in this.nodes) {
            this.nodes[i].update();
        }
    }

    this.clearCars = function () {
        for (let i=0; i < this.cars.length; i++) {
            this.cars[i].remove();
        }
        this.cars = [];
        for (let i in this.nodes) {
            this.nodes[i].parkingCarCount = 0;
            this.nodes[i].updateParking();
        }
        $('#car-count').text('0');
    }

    this.clear = function () {
        this.clearCars();
        for (let i in this.nodes){
            this.nodes[i].remove();
        }
        this.nodes = {};
        this.indexNode = 0;
    }

    this.dump = function () {
        let data = {
            nodes: [],
            cars: []
        };
        for (let i in this.nodes) {
            let node = this.nodes[i];
            let item = {
                index: node.index,
                x: node.x,
                y: node.y,
                parkingSpaceCount: node.parkingSpaceCount,
                connections: [],
                links: [],
                trafficLights: [],
            }
            for (let j = 0; j < node.connections.length; j++) {
                item.connections.push(node.connections[j].index);
            }
            for (let j in node.links) {
                let link = node.links[j];
                item.links.push({
                    index: parseInt(j, 10),
                    type: link.type,
                    maxSpeed: link.maxSpeed
                })
            }
            for (let j in node.trafficLights) {
                let trafficLight = node.trafficLights[j];
                item.trafficLights.push({
                    index: trafficLight.b.index,
                    greenTime: trafficLight.greenTime,
                    redTime: trafficLight.redTime,
                    startTime: trafficLight.startTime,
                });
            }
            data.nodes.push(item);
        }
        for (let i = 0; i < this.cars.length; i++) {
            let car = this.cars[i];
            let item = {
                route: car.getRouteIndexes(),
                speed: car.speed,
                delay: car.delay
            }
            data.cars.push(item);
        }

        return data;
    }

    this.load = function(data) {
        this.setMode('cursor');
        this.clear();
        for (let i = 0; i < data.nodes.length; i++) {
            let item = data.nodes[i];
            let node = new Node(this, item.x, item.y, item.index);
            node.parkingSpaceCount = item.parkingSpaceCount || 0;
            if (item.index > this.indexNode) {
                this.indexNode = item.index + 1;
            }
        }
        for (let i = 0; i < data.nodes.length; i++) {
            let item = data.nodes[i];
            let node = this.nodes[item.index];
            for (let j = 0; j < item.connections.length; j++) {
                node.connect(this.nodes[item.connections[j]]);
            }
            if (item.links) {
                for (let j = 0; j < item.links.length; j++) {
                    let linkItem = item.links[j];
                    let link = node.links[linkItem.index];
                    link.type = linkItem.type;
                    link.maxSpeed = linkItem.maxSpeed || 0;
                }
            }
            for (let j = 0; j < item.trafficLights.length; j++) {
                let trafficLightItem = item.trafficLights[j];
                let trafficLight = new TrafficLight(this, node, this.nodes[trafficLightItem.index]);
                trafficLight.greenTime = trafficLightItem.greenTime;
                trafficLight.redTime = trafficLightItem.redTime;
                trafficLight.startTime = trafficLightItem.startTime;
            }
        }
        if (data.cars) {
            for (let i = 0; i < data.cars.length; i++) {
                let item = data.cars[i];
                let route = [];
                for (let j = 0; j < item.route.length; j++) {
                    route.push(this.nodes[item.route[j]]);
                }
                new Car(this, route, item.speed, item.delay);
            }
        }
        this.update();
    }

    this.process = function() {
        for (let t=0; t < this.speed; t++) {
            this.tick++;
            for (let i in this.nodes) {
                let node = this.nodes[i];
                for (let j in node.trafficLights) {
                    node.trafficLights[j].process();
                }
            }
            for (let i = 0; i < this.cars.length; i++) {
                this.cars[i].process();
            }
            $('#time').text(Math.ceil(this.tick / 20));
        }
    }

    this.start = function () {
        if (this.timerId) {
            clearInterval(this.timerId);
        }
        this.tick = 0;
        for (let i in this.nodes) {
            this.nodes[i].parkingCarCount = 0;
            this.nodes[i].updateParking();
        }
        for (let i = 0; i < this.cars.length; i++) {
            let result = this.cars[i].start();
            if (result.error) {
                return {success: false, error: result.error}
            }
        }
        this.timerId = setInterval(function() {
            _this.process()
        }, 50);
        this.setMode('process');
        this.status = 'processing';
        return {success: true};
    }

    this.stop = function () {
        if (this.timerId) {
            this.status = 'pause';
            clearInterval(this.timerId);
        }
        this.setMode('cursor');
    }

    this.restore = function () {
        if (this.status === 'pause') {
            this.status = 'processing';
            this.timerId = setInterval(function () {
                _this.process()
            }, 50);
            this.setMode('process');
        }
    }

    this.addRandomCar = function () {
        let speed = randomInteger(40, 70);
        let delay = randomInteger(0, 10);
        let parkingNodes = [];
        let notParkingNodes = [];
        for (let i in this.nodes) {
            let node = this.nodes[i];
            if (node.parkingSpaceCount) {
                parkingNodes.push(node);
            }
            else{
                notParkingNodes.push(node);
            }
        }
        let lastNode = null;
        for (let i=0; i < parkingNodes.length; i++) {
            let node = parkingNodes[randomInteger(0, parkingNodes.length)];
            if (node.parkingSpaceCount > node.parkingCarCount) {
                lastNode = node;
                break;
            }
        }
        if (!lastNode) {
            console.log('No free parking spaces.');
            return;
        }
        let route = [lastNode];
        let n = randomInteger(3, 5);
        for (let i = 0; i < n; i++) {
            let exist = false;
            for (let j = 0; j < 100; j++) {
                let node = null;
                if (i === n - 1) {
                    node = parkingNodes[randomInteger(0, parkingNodes.length)];
                }
                else {
                    node = notParkingNodes[randomInteger(0, notParkingNodes.length)];
                }
                if (node !== lastNode && findAllPaths(lastNode, node, []).length > 0) {
                    exist = true;
                    route.push(node);
                    lastNode = node;
                    break;
                }
            }
            if (!exist) {
                return;
            }
        }
        new Car(this, route, speed, delay);
    }

    this.draw.on('mousedown', function(e) {
        e.preventDefault();
        let r = _this.draw.node.getBoundingClientRect();
        let x = Math.round((e.clientX - r.x) / _this.step) * _this.step;
        let y = Math.round((e.clientY - r.y) / _this.step) * _this.step;
        if (_this.mode === 'cursor' && _this.selected) {
            _this.selected.deselect();
            _this.selected = null;
        }
        if (_this.mode === 'node') {
            let exists = false;
            for (let i in _this.nodes) {
                let node = _this.nodes[i];
                if (x === node.x && y === node.y) {
                    exists = true;
                    if (_this.lastNode) {
                        _this.lastNode.connect(node);
                        _this.update();
                        _this.lastNode = null;
                    } else {
                        _this.lastNode = node;
                    }
                    break;
                }
            }
            if (!exists) {
                let node = new Node(_this, x, y);
                if (_this.lastNode) {
                    _this.lastNode.connect(node);
                }
                _this.update();
                _this.lastNode = node;
            }
        }
        else {
            _this.lastNode = null;
        }
    });

    this.draw.on('mousemove', function(e) {
        if (_this.mode === 'node' && _this.lastNode) {
            e.preventDefault();
            let r = _this.draw.node.getBoundingClientRect();
            let x = Math.round((e.clientX - r.x) / _this.step);
            let y = Math.round((e.clientY - r.y) / _this.step);
            let c = _this.lastNode.point.rbox();
            _this.line.plot(c.cx - r.x, c.cy - r.y, x * _this.step, y * _this.step).show();
        }
        else {
            _this.line.hide();
        }
    });

    this.drawGrid();
}

function Link(app, a, b) {
    let _this = this;
    this.x1 = 0;
    this.y1 = 0;
    this.x2 = 0;
    this.y2 = 0;
    this.a = a;
    this.b = b;
    this.maxSpeed = 0;
    this.type = 'asphalt';
    this.colors = {
        'asphalt': '#AAAAAA',
        'dirt': '#999966'
    };
    this.line = app.linkGroup.line(0, 0, 1, 1).stroke({width: 11, color: this.colors[this.type], linecap: 'round'});
    this.line0 = app.link0Group.line(0, 0, 1, 1).stroke({width: 1, color: 'white', linecap: 'round'});
    this.distance = getLineLength(this.a.x, this.a.y, this.b.x, this.b.y);
    a.links[b.index] = this;
    b.links[a.index] = this;
    a.connections.push(b);
    b.connections.push(a);
    a.distances[b.index] = this.distance;
    b.distances[a.index] = this.distance;
    this.refresh = function() {
        this.line0.plot(this.a.x, this.a.y, this.b.x, this.b.y);
        this.line.plot(this.a.x, this.a.y, this.b.x, this.b.y);
    }

    this.update = function() {
        let color = this.colors[this.type];
        if (app.selected === this) {
            color = 'red';
        }
        this.line.attr('stroke', color);
        this.refresh();
    }

    this.line.on('click', function() {
        if (app.mode === 'cursor') {
            _this.select();
        }
    });

    this.line.on('dblclick', function(e) {
        e.preventDefault();
        if (app.mode === 'cursor') {
            _this.select();
            $('#link-form').modal();
        }
    });

    this.select = function () {
        if (app.selected) {
            app.selected.deselect();
        }
        app.selected = this;
        _this.update();
    }

    this.deselect = function() {
        app.selected = null;
        this.update();
    }

    this.remove = function() {
        if (app.selected === this) {
            this.deselect();
            app.selected = null;
        }
        let trafficLight = this.a.trafficLights[this.b.index];
        if (trafficLight) {
            trafficLight.remove();
            delete this.a.trafficLights[this.b.index];
        }
        trafficLight = this.b.trafficLights[this.a.index];
        if (trafficLight) {
            trafficLight.remove();
            delete this.b.trafficLights[this.a.index];
        }
        removeFromList(this.a.connections, this.b);
        removeFromList(this.b.connections, this.a);
        delete this.a.links[this.b.index];
        delete this.b.links[this.a.index];
        this.line.remove();
        this.line0.remove();
    }

    this.refresh();
    this.update();
}

function TrafficLight(app, a, b) {
    this.a = a;
    this.b = b;
    this.a.trafficLights[b.index] = this;
    this.tick = 0;
    this.startTime = 0;
    this.greenTime = 0;
    this.redTime = 0;
    this.color = null;
    this.group = app.draw.group();
    this.display = this.group.set();
    this.red = this.group.circle(8).attr({fill: 'red', stroke: 'black'}).move(-4, -8).front();
    this.display.add(this.red);
    this.green = this.group.circle(8).attr({fill: 'green', stroke: 'black'}).move(-4, 0).front();
    this.display.add(this.green);

    this.update = function () {
        let angel = Math.atan2(this.b.y - this.a.y, this.b.x - this.a.x);
        angel = angel - 35 * Math.PI / 180
        let x = this.a.x + Math.cos(angel) * 20;
        let y = this.a.y + Math.sin(angel) * 20;
        this.group.move(x, y);
    }

    this.process = function () {
        this.tick++;
        if (this.tick / 20 > (this.greenTime + this.redTime)) {
            this.tick = 0;
        }
        if (this.tick / 20 <= this.greenTime) {
            this.color = 'green';
            this.green.attr('fill', 'green');
            this.red.attr('fill', 'white');
        } else {
            this.color = 'red';
            this.green.attr('fill', 'white');
            this.red.attr('fill', 'red');
        }
    }

    this.start = function () {
        if (this.startTime) {
            this.tick = this.startTime * 20;
        } else {
            this.tick = 0;
        }
    }

    this.remove = function () {
        this.group.remove();
    }

    this.update();
}


function Node(app, x, y, index) {
    var _this = this;
    if (index !== undefined) {
        this.index = index;
    }
    else {
        this.index = app.indexNode++;
    }
    this.x = 0;
    this.y = 0;
    this.f = 0;
    this.connected = false;
    this.connections = [];
    this.distances = {};
    this.links = {};
    this.path = [];
    this.hovered = false;
    this.parkingSpaceCount = 0;
    this.parkingCarCount = 0;
    this.trafficLights = {};
    this.group = app.draw.group();
    this.display = this.group.set();
    this.point = this.group.circle(10).attr({fill: '#555555'}).move(-5, -5).front();
    this.display.add(this.point);
    this.label = this.group.text('' + this.index)
        .font({size: 12, weight: 'bold'})
        .style('text-shadow', '-1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff');

    this.parkingGroup = this.group.group();
    this.display.add(this.parkingGroup.rect(15, 15).fill('#0d69e1'))
    this.display.add(this.parkingGroup.text('P')
        .font({size: 12, weight: 'bold', fill: 'white'})
        .move(4, 1));
    this.parkingLabel = this.parkingGroup.text('0')
        .font({size: 10, weight: 'bold', fill: '#0d69e1'})
        .style('text-shadow', '-1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff')
        .move(17, 15);
    this.display.add(this.parkingLabel);
    this.parkingGroup.attr({opacity: 0});
    this.parkingGroup.move(10, -10);


    this.group.draggable()
        .on('dragstart', function (e) {
            e.preventDefault();
            _this.x0 = _this.x;
            _this.y0 = _this.y;
            _this.mx = e.detail.event.clientX;
            _this.my = e.detail.event.clientY;
        })
        .on('dragmove', function (e) {
            e.preventDefault();
            if (app.mode === 'cursor') {
                //var r = draw.node.getBoundingClientRect();
                //_this.move(Math.round((e.detail.event.clientX - r.x) / step) * step, Math.round((e.detail.event.clientY - r.y) / step) * step);
                _this.move(
                    Math.round((_this.x0 + e.detail.event.clientX - _this.mx) / app.step) * app.step,
                    Math.round((_this.y0 + e.detail.event.clientY - _this.my) / app.step) * app.step
                );
            }
        });

    this.point
        .on('mouseover', function(e) {
            e.preventDefault();
            app.hoverNode = _this;
            _this.point.attr('fill', 'red');
        })
        .on('mouseout', function(e) {
            e.preventDefault();
            app.hoverNode = null;
            if (_this !== app.selected) {
                _this.point.attr('fill', '#555555');
            }
        })
        .on('mousedown', function(e) {
            e.preventDefault();
            if (app.mode === 'node') {
                if (app.lastNode) {
                    app.lastNode.connect(_this);
                    app.update();
                    app.lastNode = null;
                }
                else {
                    app.lastNode = _this;
                }
            } else if (app.mode === 'cursor') {
                _this.select();
            }
        })
        .on('dblclick', function(e) {
            e.preventDefault();
            if (app.mode === 'cursor') {
                _this.select();
                $('#node-form').modal();
            }
        });

    this.connect = function(node) {
        if (this === node || this.connections.indexOf(node) >= 0)
            return false;
        new Link(app, this, node);
        return true;
    }

    this.refresh = function() {
        let p = this.point.rbox();
        let r = app.draw.node.getBoundingClientRect();
        this.x = p.cx - r.x
        this.y = p.cy - r.y;
        for (let i in this.links) {
            this.links[i].refresh();
        }
        for (let i in this.trafficLights) {
            this.trafficLights[i].update();
        }
    }

    this.remove = function() {
        for (let i in this.links) {
            this.links[i].remove();
        }
        this.links = {}
        for (let i in this.trafficLights) {
            this.trafficLights[i].remove();
        }
        this.trafficLights = {};
        this.group.remove();
        if (app.selected === this) {
            app.selected = null;
        }

    }

    this.getStartParkingCarCount = function () {
        let count = 0;
        for (let i = 0; i < app.cars.length; i++) {
            if (app.cars[i].route[0] === this) {
                count++;
            }
        }
        return count;
    }

    this.updateParking = function () {
        if (this.parkingSpaceCount) {
            this.parkingGroup.attr({ opacity: 1 });
            this.parkingLabel.plain(this.parkingCarCount + ' / ' + this.parkingSpaceCount);
            if (this.parkingSpaceCount > this.parkingCarCount) {
                this.parkingLabel.font({fill: '#0d69e1'});
            } else {
                this.parkingLabel.font({fill: '#cc0000'});
            }
        } else {
            this.parkingGroup.attr({ opacity: 0 });
        }
    }

    this.update = function() {
        let fill = '#555555';
        if (app.selected && app.selected === this) {
            fill = 'red';
        }
        this.point.attr({'fill': fill});
        for (let i in this.links) {
            this.links[i].update();
        }
        this.updateParking();
    }

    this.move = function(x, y) {
        if (x !== this.x || y !== this.y) {
            this.x = x;
            this.y = y;
            this.group.translate(x, y);
            this.refresh();
        }
    }

    this.select = function () {
        if (app.selected) {
            app.selected.deselect();
        }
        app.selected = this;
        this.update();
    }

    this.deselect = function () {
        app.selected = null;
        this.update();
    }

    app.nodes[this.index] = this;

    this.move(x, y);
    this.refresh();
    this.update();
}

function Car(app, route, speed, delay) {
    var _this = this;
    this.route = route;
    this.routeIndex = 0;
    this.speed = speed;
    this.currentSpeed = 0;
    this.distance = 0;
    this.delay = delay;
    this.a = null;
    this.b = null;
    this.d = 0;
    this.path = [];
    this.pathIndex = 0;
    this.colors = ['#003366', '#0000cc', '#6600cc', '#660066', '#990033', '#663300', '#336600', '#003300'];
    this.color = this.colors[randomInteger(0, this.colors.length)];
    this.group = app.draw.group();
    this.display = this.group.set();
    this.display.add(this.group.rect(10, 6).fill(this.color).move(-5, -6));
    this.display.add(this.group.circle(2).attr({fill: 'yellow'}).move(-5, -2));
    this.display.add(this.group.circle(2).attr({fill: 'yellow'}).move(-5, -6));
    this.display.add(this.group.circle(2).attr({fill: 'red'}).move(3, -2));
    this.display.add(this.group.circle(2).attr({fill: 'red'}).move(3, -6));
    this.toast = null;
    this.active = false;
    this.motion = false;
    this.route[0].parkingCarCount++;
    this.route[0].updateParking();

    this.getRouteIndexes = function () {
        let indexes = [];
        for (let i = 0; i < this.route.length; i++) {
            indexes.push(this.route[i].index);
        }
        return indexes;
    }

    this.move = function(x, y) {
        if (x !== this.x || y !== this.y) {
            this.x = x;
            this.y = y;
            this.group.translate(x, y);
        }
    }

    this.rotate = function () {
        if (this.a && this.b) {
            let angel = Math.atan2(this.b.y - this.a.y, this.b.x - this.a.x) * 180 / Math.PI;
            this.display.rotate(angel + 180, 0, 0);
        }
    }

    this.process = function() {
        if (!this.active) {
            return;
        }
        if (this.delay && app.tick < this.delay*20) {
            return;
        }
        if (!this.motion) {
            this.motion = true;
            this.move(this.a.x, this.a.y);
            this.rotate();
            this.group.attr({ opacity: 1 });
            this.a.parkingCarCount--;
            this.a.updateParking();
        }
        let speed = this.speed / 20;
        let maxSpeed = this.a.links[this.b.index].maxSpeed;
        if (maxSpeed && speed > maxSpeed / 20) {
            speed = maxSpeed / 20;
        }
        for (let i = 0; i < app.cars.length; i++) {
            let car = app.cars[i];
            if (this !== car && car.motion && this.a === car.a && this.b === car.b && this.d < car.d) {
                if ((car.d - (this.d + speed)) < 15) {
                    speed = car.d - this.d - 15;
                    break;
                }
            }
        }
        if (speed <= 0) {
            this.currentSpeed = 0;
            this.updateToast();
            return;
        }
        this.currentSpeed = speed * 20;
        let vx = this.b.x - this.a.x;
        let vy = this.b.y - this.a.y;
        let vl = Math.sqrt(vx*vx + vy*vy);
        vx = vx / vl;
        vy = vy / vl;
        let x = this.x + vx * speed;
        let y = this.y + vy * speed;
        let d = getLineLength(this.a.x, this.a.y, x, y);
        let m = getLineLength(this.a.x, this.a.y, this.b.x, this.b.y) - d;
        if (m <= 0) {
            x = this.b.x;
            y = this.b.y;
            if (this.pathIndex < (this.path.length - 2)) {
                this.pathIndex++;
                this.a = this.path[this.pathIndex];
                this.b = this.path[this.pathIndex + 1];
                this.d = 0;
                this.move(x, y);
                this.rotate();
            } else {
                if (this.routeIndex < (this.route.length - 2)) {
                    this.routeIndex++;
                    this.path = getMinPath(findAllPaths(this.route[this.routeIndex], this.route[this.routeIndex + 1], []));
                    if (this.path) {
                        this.pathIndex = 0;
                        this.a = this.path[0];
                        this.b = this.path[1];
                        this.d = 0;
                        this.move(this.a.x, this.a.y);
                        this.rotate();
                    } else {
                        this.active = false;
                        this.motion = false;
                        this.currentSpeed = 0;
                        this.group.animate({
                          duration: 1000
                        }).attr({ opacity: 0 });
                    }
                }
                else {
                    if (this.b.parkingSpaceCount > this.b.parkingCarCount) {
                        this.b.parkingCarCount++;
                        this.b.updateParking();
                        this.currentSpeed = 0;
                        this.active = false;
                        this.motion = false;
                        this.group.animate({
                            duration: 1000
                        }).attr({opacity: 0});
                    } else {
                        console.log('No parking spaces');
                        this.currentSpeed = 0;
                    }
                }
            }
        } else {
            let trafficLight = this.b.trafficLights[this.a.index];
            if (m > 20 || !trafficLight || trafficLight.color === 'green') {
                this.d = d;
                this.move(x, y);
                this.distance += this.currentSpeed / 3600 / 20;
            } else {
                this.currentSpeed = 0;
            }
        }
        this.updateToast();
    }

    this.start = function() {
        this.routeIndex = 0;
        this.pathIndex = 0;
        this.path = getMinPath(findAllPaths(this.route[0], this.route[1], []));
        if (this.path) {
            this.a = this.path[0];
            this.b = this.path[1];
            this.d = 0;
            this.a.parkingCarCount++;
            this.a.updateParking();
            this.active = true;
            this.motion = false;
            this.move(this.a.x, this.a.y);
            this.rotate();
            this.group.attr({ opacity: 0 });
            this.distance = 0;
            this.currentSpeed = 0;
            return {success: true}
        } else{
            this.motion = false;
            return {success: false, error: 'Не можливо побудувати маршрут'};
        }
    }

    this.remove = function() {
        this.group.remove();
    }

    this.updateToast = function() {
        if (this.toast) {
            this.toast.find('.current-speed').text(this.currentSpeed);
            this.toast.find('.distance').text(Math.round(this.distance * 100) / 100);
        }
    }

    app.cars.push(this);
    $('#car-count').text(app.cars.length);

    this.group.on('dblclick', function(e) {
        e.preventDefault();
        if (_this.toast) {
            return;
        }
        $('#toasts').append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"></div>');
        _this.toast = $('#toasts .toast').last();
        _this.toast.html(
            '<div class="toast-header">' +
                '<strong class="mr-auto">Автомобіль ' + app.cars.indexOf(_this) + '</strong>' +
                '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>' +
            '<div class="toast-body">' +
                '<div>Маршрут: ' + _this.getRouteIndexes().join(', ') + '</div>' +
                '<div>Звичайна швидкість: ' + _this.speed + ' км/год</div>' +
                '<div>Поточна швидкість: <span class="current-speed"></span> км/год</div>' +
                '<div>Пройдений шлях: <span class="distance"></span> км</div>' +
            '</div>');
        _this.updateToast();
        _this.toast.toast('show');
        _this.toast.on('hide.bs.toast', function () {
            _this.toast = null;
        });
    });
}
